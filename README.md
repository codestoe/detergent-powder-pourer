# Detergent Powder Pourer

This is an [OpenSCAD](https://openscad.org/) Project for 3d-printing a pot for detergent powder. The lid has a nozzle to make pouring the powder into the dishwasher easier.

## Getting started

There are three parts of the pourer. The pot, the lid and the nozzle, which can rendered manually by setting the Part-variable.


## Authors and acknowledgment


## License
GPL 3

## Project status
First prototype is in production and working as inteded. At the time of the first commit the learnings like nozzle radius, wall width, and pot diameter, are alredy added.