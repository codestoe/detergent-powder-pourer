include <BOSL2/std.scad>
include <BOSL2/threading.scad>

$fn=100;

// base == 0, lid == 1, nozzle == 2
Part=0;

wallWidth=3.5;
height=200;
diameter=110;

lidHeight=20;
baseHeight=height-lidHeight;

lidThreadLength=12;
threadPitch=4;

outerRadius=10;
pourerHeight=20;
pourerWallWidth=3;
pourerTrheadPitch=2;

module nozzle(volume_ml=30) {
    up(height/2+pourerHeight/2-wallWidth/2)
    right(diameter/2-wallWidth-2*outerRadius)
    difference(){
    union()
    {
        down(pourerHeight/2-.1)
        threaded_rod(l=wallWidth, d=2*outerRadius, pitch=pourerTrheadPitch, lead_in=10);
        cyl(h=pourerHeight-wallWidth, r=outerRadius, rounding2=1);

//    volume_mm3=volume_ml*1000;
//    sphereInnerRadius= ((3*volume_mm3)/(4*PI))^(1/3);
//        up(pourerHeight)
//        difference(){
//            sphere(r=sphereInnerRadius+pourerWallWidth);
//            sphere(r=sphereInnerRadius);
//        }
    }
    down(wallWidth/2)
    cyl(h=pourerHeight+.1, r=outerRadius-pourerWallWidth, rounding2=-1);
    }
}

if(Part != 2)
{
union(){
    difference(){
        union(){
        cyl(h=height, d=diameter, rounding=10);
//        if (Part==1)    
//            nozzle();
        }
        
        
        if (Part==0) 
        {
            cyl(h=height-2*wallWidth, d=diameter-2*wallWidth, rounding=8);
            up(height/2-lidHeight/2+.01)
            cube([diameter, diameter, lidHeight], center=true);
            up(height/2-lidHeight/2-lidThreadLength)
            threaded_rod(d=diameter-wallWidth, l=lidThreadLength, pitch=threadPitch, internal=true, lead_in2=30);
        }

        if (Part==1) 
        {
            cyl(h=height-2*wallWidth, d=diameter-2*wallWidth-8, rounding=8);
            down(height/2-baseHeight/2+.1)
            cube([diameter, diameter, baseHeight], center=true);
        }
        
        //nozzle hole
        up(height/2-wallWidth/2-.1)
        right(diameter/2-wallWidth-2*outerRadius)
        threaded_rod(l=wallWidth+.3,d=2*outerRadius, pitch=pourerTrheadPitch, internal=true);
    }
    if (Part==1) 
    {
        difference(){
            up(height/2-lidHeight/2-lidThreadLength+.1)
            threaded_rod(d=diameter-wallWidth-1, l=lidThreadLength, pitch=threadPitch, lead_in=30);
            
            cyl(h=height-2*wallWidth, d=diameter-2*wallWidth-8, rounding=8); 
        }
    }
}
} else 
{
    nozzle();
}